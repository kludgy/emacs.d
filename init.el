;; base emacs-24.5

;; a location for ad-hoc el files
(add-to-list 'load-path "~/.emacs.d/lisp/use-package/")
(add-to-list 'load-path "~/.emacs.d/lisp/")


;; use stable MELPA repo
(require 'package)
(add-to-list 'package-archives
         '("melpa" . "http://melpa-stable.milkbox.net/packages/"))
(package-initialize)


;; sync packages
(require 'use-package)
(setq use-package-always-ensure t)
(use-package haskell-mode)
(use-package intero)
(use-package projectile)
(use-package helm)
(use-package helm-ag)
(use-package helm-ghc)
(use-package helm-projectile)
(use-package idris-mode)
(use-package ace-jump-mode)
(use-package rainbow-delimiters)
(use-package magit)
(use-package web-mode)
(use-package multiple-cursors)


;; enable recent files cache
(require 'recentf)
(recentf-mode 1)
(setq recentf-max-menu-items 200)


;; No large file warning. Eliminates annoying large TAGS file prompt.
(setq large-file-warning-threshold nil)


;; multiple-cursors config
(global-set-key (kbd "C-S-c C-S-c") 'mc/edit-lines)
(global-set-key (kbd "C->") 'mc/mark-next-like-this)
(global-set-key (kbd "C-<") 'mc/mark-previous-like-this)
(global-set-key (kbd "C-c C-<") 'mc/mark-all-like-this)

;; helm/projectile config
(require 'helm-config)
(setq projectile-indexing-method 'alien)
(setq projectile-enable-caching t)
(setq projectile-completion-system 'helm)
(projectile-global-mode)

;; disable frequent (very slow) projectile file discovery when in remote (tramp) session
(defadvice projectile-project-root (around ignore-remote first activate)
  (unless (file-remote-p default-directory) ad-do-it))

(helm-projectile-on)


;; to solve the problem of new buffers being created with windows line endings in mirrored
;; unix directories, default to utf-8-unix when a dominant file named .unix-mirror is located

(defun windows-nt-force-unix-line-endings ()
  "to be added as a find-file-hook in windows-nt"
  (if (locate-dominating-file buffer-file-name ".unix-mirror")
      (setq buffer-file-coding-system 'utf-8-unix)))

(when (string-equal system-type "windows-nt") (add-hook 'find-file-hook 'windows-nt-force-unix-line-endings))


;; web dev
(require 'web-mode)
(add-to-list 'auto-mode-alist '("\\.phtml\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.tpl\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.php\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.[agj]sp\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.as[cp]x\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.erb\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.mustache\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.djhtml\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.html?\\'" . web-mode))


;; idris
(require 'idris-mode)


;; haskell
(add-hook 'haskell-mode-hook 'intero-mode)


;; copy file path
(defun copy-file-name-to-clipboard ()
  "Copy the current buffer file name to the clipboard."
  (interactive)
  (let ((filename (if (equal major-mode 'dired-mode)
                      default-directory
                    (buffer-file-name))))
    (when filename
      (kill-new filename)
      (message "Copied buffer file name '%s' to the clipboard." filename))))


;; try to automatically configure indentation
(which-function-mode)
(require 'fuzzy-format)
(setq fuzzy-format-default-indent-tabs-mode nil)
(global-fuzzy-format-mode t)


;; general programming
(add-to-list 'auto-mode-alist '("\\.h\\'" . c++-mode))
(add-to-list 'auto-mode-alist '("\\.hh\\'" . c++-mode))
(add-to-list 'auto-mode-alist '("\\.inl\\'" . c++-mode))
(add-to-list 'auto-mode-alist '("\\.cc\\'" . c++-mode))
(add-hook 'prog-mode-hook #'rainbow-delimiters-mode)
(setq-default c-basic-offset 4)
(setq-default tab-width 4)
(setq-default c-default-style "linux")
(setq
   nxml-child-indent 4
   nxml-attribute-indent 4
   nxml-slash-auto-complete-flag t)
(require 'p4)
(global-auto-revert-mode 1)
(c-set-offset 'case-label '+)


;; This hack fixes indentation for C++11's "enum class" in Emacs.
;; http://stackoverflow.com/questions/6497374/emacs-cc-mode-indentation-problem-with-c0x-enum-class/6550361#6550361

(defun inside-class-enum-p (pos)
  "Checks if POS is within the braces of a C++ \"enum class\"."
  (ignore-errors
    (save-excursion
      (goto-char pos)
      (up-list -1)
      (backward-sexp 1)
      (or (looking-back "enum\\s-+class\\s-+")
          (looking-back "enum\\s-+struct\\s-+")
          (looking-back "enum\\s-+class\\s-+\\S-+\\s-*:\\s-*")
          (looking-back "enum\\s-+struct\\s-+\\S-+\\s-*:\\s-*")))))

(defun align-enum-class (langelem)
  (if (inside-class-enum-p (c-langelem-pos langelem))
      0
    (c-lineup-topmost-intro-cont langelem)))

(defun align-enum-class-closing-brace (langelem)
  (if (inside-class-enum-p (c-langelem-pos langelem))
      '-
    '*))

(defun fix-enum-class ()
  "Setup `c++-mode' to better handle \"class enum\"."
  (add-to-list 'c-offsets-alist '(topmost-intro-cont . align-enum-class))
  (add-to-list 'c-offsets-alist
               '(statement-cont . align-enum-class-closing-brace)))

(add-hook 'c++-mode-hook 'fix-enum-class)


;; c++-mode
(defun my-c++-mode-hook ()
  (c-set-offset 'substatement-open 0)
  (c-set-offset 'inline-open 0))
(add-hook 'c++-mode-hook 'my-c++-mode-hook)


;; ace jump
;;(add-to-list 'load-path "~/emacs.d/elpa/ace-jump-mode-2.0/ace-jump-mode.el/in/")
(autoload
  'ace-jump-mode
  "ace-jump-mode"
  "Emacs quick move minor mode"
  t)
(define-key global-map (kbd "C-c SPC") 'ace-jump-mode)

(autoload
  'ace-jump-mode-pop-mark
  "ace-jump-mode"
  "Ace jump back:-)"
  t)
(eval-after-load "ace-jump-mode"
  '(ace-jump-mode-enable-mark-sync))
(define-key global-map (kbd "C-x SPC") 'ace-jump-mode-pop-mark)


;; tramp ssh debugging
(setq tramp-default-method "plink")
(setq tramp-verbose 0)


(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-color-faces-vector
   [default default default italic underline success warning error])
 '(ansi-color-names-vector
   ["black" "#d55e00" "#009e73" "#f8ec59" "#0072b2" "#cc79a7" "#56b4e9" "white"])
 '(column-number-mode t)
 '(compilation-message-face (quote default))
 '(custom-enabled-themes (quote (deeper-blue)))
 '(custom-safe-themes
   (quote
	("0c49a9e22e333f260126e4a48539a7ad6e8209ddda13c0310c8811094295b3a3" default)))
 '(fci-rule-color "#49483E")
 '(haskell-process-type (quote stack-ghci))
 '(helm-ag-insert-at-point (quote symbol))
 '(highlight-changes-colors (quote ("#FD5FF0" "#AE81FF")))
 '(highlight-tail-colors
   (quote
	(("#49483E" . 0)
	 ("#67930F" . 20)
	 ("#349B8D" . 30)
	 ("#21889B" . 50)
	 ("#968B26" . 60)
	 ("#A45E0A" . 70)
	 ("#A41F99" . 85)
	 ("#49483E" . 100))))
 '(inhibit-startup-screen t)
 '(magit-diff-use-overlays nil)
 '(menu-bar-mode nil)
 '(package-selected-packages
   (quote
	(web-mode magit rainbow-delimiters helm-projectile helm-ghc helm-ag flycheck-haskell ace-jump-mode)))
 '(scroll-bar-mode nil)
 '(tool-bar-mode nil)
 '(vc-annotate-background nil)
 '(vc-annotate-color-map
   (quote
	((20 . "#F92672")
	 (40 . "#CF4F1F")
	 (60 . "#C26C0F")
	 (80 . "#E6DB74")
	 (100 . "#AB8C00")
	 (120 . "#A18F00")
	 (140 . "#989200")
	 (160 . "#8E9500")
	 (180 . "#A6E22E")
	 (200 . "#729A1E")
	 (220 . "#609C3C")
	 (240 . "#4E9D5B")
	 (260 . "#3C9F79")
	 (280 . "#A1EFE4")
	 (300 . "#299BA6")
	 (320 . "#2896B5")
	 (340 . "#2790C3")
	 (360 . "#66D9EF"))))
 '(vc-annotate-very-old-color nil)
 '(weechat-color-list
   (quote
	(unspecified "#272822" "#49483E" "#A20C41" "#F92672" "#67930F" "#A6E22E" "#968B26" "#E6DB74" "#21889B" "#66D9EF" "#A41F99" "#FD5FF0" "#349B8D" "#A1EFE4" "#F8F8F2" "#F8F8F0"))))

(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(rainbow-delimiters-depth-2-face ((t (:foreground "#5378e6"))))
 '(rainbow-delimiters-depth-4-face ((t (:foreground "#50b850"))))
 '(rainbow-delimiters-depth-5-face ((t (:foreground "#b05353"))))
 '(rainbow-delimiters-depth-6-face ((t (:foreground "#5378e6"))))
 '(rainbow-delimiters-depth-8-face ((t (:foreground "#50b850"))))
 '(rainbow-delimiters-depth-9-face ((t (:foreground "#b05353")))))


;; adhoc config
(cond
 
 ((string-equal system-type "windows-nt")
  (cond
   ((equal (nth 1 (split-string (shell-command-to-string "wmic desktopmonitor get caption") "  \r\n"))
           "Surface Display")
    (set-default-font "Consolas-10"))
   (t
    (set-default-font "Consolas-8")))
  (setq-default line-spacing 0)
  (setq helm-ag-base-command "pt -e --nocolor --nogroup"))
 
 ((string-equal system-type "gnu/linux")
  (set-default-font "Terminus-9")
  (setq helm-ag-base-command "ag --nocolor --nogroup")))
